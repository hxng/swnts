<p><img style = "padding: 0 15px; float: left;" src = logo/SchwannomatosisOpenResearchCollaborativeBanner-V1.png></p>

**Schwannomatosis** is a rare, understudied genetic disorder that predisposes individuals to the development of nerve sheath tumors. While schwannomatosis is in some cases caused by germline mutations in the _SMARCB1_ and _LZTR1_ genes, the genomic landscape and genetic modifiers of this disorder are poorly understood. Recent efforts to study the genomics of schwannomatosis patients have increased our understanding of the disorder, and much of the data from this research is now publicly available. With support from the Children's Tumor Foundation, we have developed the **Schwnnomatosis Open Research Collaborative**, a community-based research project with the goal of utilizing public data to uncover new scientific insights into schwannomatosis. The primary objective of this project is to perform comprehensive genomic analyses to uncover **noncoding variants**, **coding variants in under-studied genes**, or other genomic features that contribute to disease etioogy and heterogeneity. To learn more about schwannomatosis, see the [Further Reading](https://gitlab.com/-/ide/project/nf-community/swnts/edit/main/-/README.md#further-reading) section below. 

## Available Data

This project utilizes data from [Synodos for Schwannomatosis initiative](https://nf.synapse.org/Explore/Studies/DetailsPage?studyId=syn9727752) by [Mansouri et al.](https://doi.org/10.1007/s00401-020-02230-x) Instructions on how to obtained access to the processed data are available on the project [Synapse page](https://www.synapse.org/SWNTS).

## How to participate

Please fill out this [registration form](https://docs.google.com/forms/d/e/1FAIpQLSd-lL6x2ub5LAjbzsK1469KWca6qhSEA24lMUJ_QkCxdeSEOA/viewform?usp=sf_link) to join the collaborative. Doing so will insure that you receive proper credit for your contributions in publications and any other materials produced as a result of this effort.

### Planned analyses

There are certain analyses that we have planned or that others have proposed, but which nobody is currently in charge of completing. Check the existing [issues](https://gitlab.com/nf-community/swnts/-/issues?sort=created_date&state=opened) to identify these. If you would like to take on a planned analysis, please comment on the issue noting your interest. Ask clarifying questions to understand the current scope and goals then propose a potential solution. If the solution aligns with the goals, we will ask you to go ahead and implement the solution. You should provide updates to your progress in the issue. When you file a merge request with your solution, you should note that it closes the issue in question.

### Proposing a new analysis

In addition to the planned analyses, we welcome contributors who wish to propose their own analyses of this dataset as part of the SWNTS project. Check the existing [issues](https://gitlab.com/nf-community/swnts/-/issues?sort=created_date&state=opened) before proposing an analysis to see if something similar is already planned. If there is not a similar planned analysis, create a new issue. The ideal issue will describe the scientific goals of the analysis, the planned methods to address the scientific goals, the input data that is required for the planned methods, and a proposed timeline for the analysis. We have created an [issue template](https://gitlab.com/nf-community/swnts/-/blob/main/.gitlab/issue_templates/propose-an-analysis.md) to help you get started. Project maintainers will interact on the issue to clarify any questions or raise potential concerns.

### Joining an in-progress analysis

If you find an analysis in the existing [issues](https://gitlab.com/nf-community/swnts/-/issues) that appears to be in progress, but feel that you can contribute to that analysis in a meaningful way, please comment on that issue with your suggestions and indicate your desire to be involved. Project maintainers will interact on the issue to clarify what is still needed on the analysis and how you might be able to contribute.

### Implementing an analysis
***
This section describes the general workflow for implementing analytical code, and more details are provided in the [project wiki](https://gitlab.com/nf-community/swnts/-/wikis/home#add-an-analysis).
The first step is to identify an existing analysis or propose a new analysis, engage with the project maintainers to clarify the goals of the analysis, and then get the go ahead to move forward with the analysis.

#### Analytical Code and Output

You can perform your analyses via a script (R or Python) or via a notebook (R Markdown or Jupyter).
Your analyses should produce one or more *artifacts*.
Artifacts include both vector or high-resolution figures sufficient for inclusion in a manuscript as well as new summarizations of the data (tables, etc) that are intended for either use in subsequent analyses or distribution with the manuscript.

#### Software Dependencies

For reproducibility purposes, we require that each analysis be developed within a Docker container that includes all necessary dependencies. We have provided a template [`Dockerfile`](https://gitlab.com/nf-community/swnts/-/blob/main/Dockerfile) in this repository that can be used as a starting point. If you need help creating or using a Docker image for your analyses, please [file a new issue](https://gitlab.com/nf-community/swnts/-/issues/new) on this repository.


#### Merge Request Model

Analyses are added to this repository via merge requests (also known as pull requests).
**Please read the [merge request section of the contribution guidelines](https://gitlab.com/nf-community/swnts/-/blob/main/CONTRIBUTING.md) carefully. More detailed instructions on creating merge requests can be found on the [project wiki](https://gitlab.com/nf-community/swnts/-/wikis/home#create-a-merge-request).**

## Code of conduct

This project adheres to the Contributor Covenant [code of conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to the project organizers at nf-osi@sagebionetworks.org.

## Authorship

The ultimate goal of this is effort is to describe the results of our analyses in a manuscript.
We will use the [ICMJE Guidelines](http://www.icmje.org/recommendations/browse/roles-and-responsibilities/defining-the-role-of-authors-and-contributors.html).
We expect authors to contribute to the overall design of the project by participating in issues, to contribute analyses via filing pull requests that integrate analyses into this repository, to contribute to the text by contributing sections and/or revisions to a manuscript.
It is important to note that, for authorship, these should be substantial intellectual contributions.

## Questions

If you have questions about this project that have not been answered in these documents, please email nf-osi@sagebionetworks.org

## Further Reading

- [Schwannomatosis primer (NCATS Genetic and Rare Diseases Information Center)](https://rarediseases.info.nih.gov/diseases/4768/schwannomatosis)
- Synodos Schwannomatosis publication: 
> Mansouri, S., Suppiah, S., Mamatjan, Y. et al. Epigenomic, genomic, and transcriptomic landscape of schwannomatosis. Acta Neuropathol 141, 101–116 (2021). https://doi.org/10.1007/s00401-020-02230-x

## Acknowledgements

This project is supported by [Sage Bionetworks](https://sagebionetworks.org/) and the [Children's Tumor Foundation](https://www.ctf.org/). This project is modeled after the [Open Pediatric Brain Tumor Atlas (OpenPBTA)](https://www.ccdatalab.org/openpbta) and much of the content for this project has been adapted from the [OpenPBTA GitHub page](https://github.com/AlexsLemonade/OpenPBTA-analysis). The data powering this study was originally generated by _Mansouri et. al. Acta Neuropathol. 2021._ Banner designed by Stockard Simon.
***
