

datadir="../../data/WES_Processed/VariantCalling"

#Create list of samples for later use
ls $datadir | grep blood | grep -v tumor > ../../scratch/samples.txt


###BLOOD only
# Note, as written, this only keeps PASS filter values from DeepVariant,
# but keeps all HaplotypeCaller calls (no filter info available)
while read -r sample ; do
	#Paths to vcfs
	f1=${datadir}/${sample}/DeepVariant/${sample}.vcf.gz
	f2=${datadir}/${sample}/HaplotypeCaller/HaplotypeCaller_${sample}.vcf.gz
	#Keep only PASS variants from DeepVariant
	zcat ${f1} | egrep "#|PASS" > ../../scratch/${sample}.DV.vcf
	#Keep all variants from HaplotypeCaller (no filter info present)
	zcat ${f2} > ../../scratch/${sample}.HC.vcf # Appears to have no filter info
	vcftools --vcf ../../scratch/${sample}.DV.vcf --diff ../../scratch/${sample}.HC.vcf --diff-site --out ../../scratch/${sample}.DV1_v_HC2.out
done < ../../scratch/samples.txt

