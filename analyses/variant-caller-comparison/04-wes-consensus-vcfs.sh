
dir="../../scratch"

# Use vcftool's vcf-merge tool to get consensus germline calls from  
# DeepVariant and HaplotypeCaller 
# In 01-preprocess-data.sh, copies of their vcfs  were placed in scratch
# and only include PASSed variants.



while read -r sample; do
	echo $sample
	#vcf-merge requires tabix indexed files. Tabix requires bgzipped files
	bgzip -c ${dir}/${sample}.DV.vcf > ${dir}/${sample}.DV.vcf.gz
	bgzip -c ${dir}/${sample}.HC.vcf > ${dir}/${sample}.HC.vcf.gz
	tabix ${dir}/${sample}.DV.vcf.gz
	tabix ${dir}/${sample}.HC.vcf.gz
	
	#Merge the vcfs
	#vcf-merge prints all headers in both vcfs, causing many duplicates.
	#vcf-merge also prints all variants, not just conensus calls.
	#awk statement prints only unique header lines and variants
	#which were reported by both DeepVariant (field 10) and HaplotypeCaller (field 11)
	vcf-merge ../../scratch/${sample}.DV.vcf.gz ../../scratch/${sample}.HC.vcf.gz \
	 | awk '{if($1~"#" && !seen[$0]++)print}FS=OFS="\t" {if($10!="." && $11!="." && $1 !~ "#") print}' \
	 > ../../scratch/${sample}.DV.HC.consensus.vcf
done < ${dir}/samples.txt
