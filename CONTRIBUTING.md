# Contribution guidelines for the Schwannomatosis Open Research Collaborative

## Issues

The best place to get involved with the project is likely to be via cration of a new issue or particpation in existing issues. We use issues to propose and discuss analyses, as well as to adress challenges interacting with the data or other roadblocks. Participants who wish to perform an analysis of the data as part of this effort should either identify an existing planned analysis that they wish to tackle or perform a new one. 

## Merge Requests

Contributions to this project operate on a merge request model (also known as a pull request model). We expect participants to actively review merge requests, especially for requests that include analyses within their areas of expertise. See the [project wiki](https://gitlab.com/nf-community/swnts/-/wikis/home#create-a-merge-request) for more details on how to create a merge request.

### Size and composition of merge requests

The ideal merge request is small enough for reviewers to review the code in detail and focused on a single area or, if adding a new file entirely, a single file. Implementing an analysis will often require more than one notebook or script to be added to the repository. It is best to submit multiple merge requests for these analyses rather than a single, large merge request when an analysis is completed. This facilitates scientific discussion and reduces the burden on reviewers (see [Peer review](#peer-review)).

### Peer review

All merge requests will undergo peer review. Participants in this project should review merge requests. They should suggest modifications or directly edit the merge request to make suggested changes. As a reviewer, it is helpful to note the type of review you performed: did you look over the source code, did you run the source code, did you run the source code, did you look at and interpret the results of a combination of these.

Before a repository maintainer approves a merge request, there must be at least one affirmative review. If there is any unaddressed criticism or disapproval, a repository maintainer will determine how to proceed and may wait for additional feedback.

## Authorship

The ultimate goal of this effort is to describe our analyses in a manuscript. We will use the [ICMJE Guidelines](http://www.icmje.org/recommendations/browse/roles-and-responsibilities/defining-the-role-of-authors-and-contributors.html) to determine authorship. We expect authors to contribute to the overall design of the project by participating in issues, to contribute analyses by filing merge requests that integrate analyses into this repository, to contribute to the text by contributing sections and/or revisions to the manuscript once it is in progress. An individual must make substantial intellectual contributions in order to receive authorship.
